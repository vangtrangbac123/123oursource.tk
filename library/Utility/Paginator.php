<?php

class Utility_Paginator
{
    public static function create($page_link, $active_page, $items_per_page, $total_items, $visible_pages_pad = 2) {

        if ($items_per_page < 1 || $active_page < 1 || !$visible_pages_pad) return false;

        $max_page           = (int)ceil($total_items / $items_per_page);
        $active_page        = min($active_page, $max_page);
        $active_page        = max($active_page, 1);
        $total_current_item = min($items_per_page * $active_page, $total_items);

        $data = array();
        $data['pageLink']         = $page_link;
        $data['totalItemCount']   = $total_items;
        $data['pageCount']        = $max_page;
        $data['itemCountPerPage'] = $items_per_page;
        $data['current']          = $active_page;
        $data['first']            = 1;
        $data['last']             = $max_page;
        $data['prev']             = max($active_page - 1, 1);
        $data['next']             = min($active_page + 1, $max_page);
        $data['currentItemCount'] = $items_per_page;
        $data['firstItemNumber']  = min($total_current_item - $items_per_page + 1, $total_items);
        $data['firstItemNumber']  = max($data['firstItemNumber'], 1);
        $data['lastItemNumber']   = $total_current_item;

        $visible_pages = $visible_pages_pad * 2 + 1;
        if ($max_page <= $visible_pages) {
            $minp = 1;
            $maxp = $max_page;
        } else {
            $minp = $active_page - $visible_pages_pad;
            if ($minp < 1) $minp = 1;
            $maxp = $minp + $visible_pages - 1;
            if ($maxp > $max_page) {
                $maxp = $max_page;
                $minp = $maxp - $visible_pages + 1;
            }
        }

        $data['pagesInRange'] = array();
        for ($i = $minp; $i <= $maxp; $i++) {
            $data['pagesInRange'][] = $i;
        }
        $data['firstPageInRange'] = $minp;
        $data['lastPageInRange']  = $maxp;

        return (object)$data;
    }
}
