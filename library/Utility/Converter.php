<?php

class Utility_Converter
{
    public static function arrayToObject($array)
    {
        if (is_array($array)) {
            $obj = new StdClass();

            foreach ($array as $key => $val) {
                $obj->$key = $val;
            }
        } else {
            $obj = $array;
        }

        return $obj;
    }

    public static function objectToArray($object)
    {
        if (is_object($object)) {

            foreach ($object as $key => $value) {
                $array[$key] = $value;
            }
        } else {
            $array = $object;
        }
        return $array;
    }

    public static function arrayFilter($array, $fields)
    {
        if (is_object($array)) $array = (array)$array;
        foreach ($array as $key => $value) {
            if (!in_array($key, $fields)) {
                unset($array[$key]);
            }
        }
        return $array;
    }

    public static function toMemorySize($size)
    {
        $unit = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
        return @round($size/pow(1024,($i = floor(log($size,1024)))),2) . ' ' . (isset($unit[$i]) ? $unit[$i] : 'B');
    }

    public static function download_csv_results($results, $name)
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename='. $name);
        header('Pragma: no-cache');
        header("Expires: 0");

        $outstream = fopen("php://output", "w");
        fputcsv($outstream, array(  'Date add',
                                    'Ticket Code',
                                    'Session',
                                    'Name',
                                    'Phone',
                                    'Email',
                                    'Cinema',
                                    'Film',
                                    'Seat',
                                    'Price (VND)'));

        foreach($results as $result)
        {
            fputcsv($outstream, $result);
        }

        fclose($outstream);
    }
}

