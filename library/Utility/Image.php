<?php

class Utility_Image {

    /**
     * @param  $_FILES $file
     * @return string $imageUrl | False
     */
    public static function uploadImageToServer($file) {

        try {

            // prepare
            $fileName = pathinfo($file['name'], PATHINFO_FILENAME); // PHP 5.2.0

            // read file
            $handle = fopen($file['tmp_name'], 'r');
            $fileData = fread($handle, $file['size']);
            fclose($handle);

            // request data
            $params = array(
                'file_data' => $fileData,
                'file_name' => $fileName,
                'seckey'    => md5($fileName . Config::API_IMG_KEY),
            );

            // request
            $ch = curl_init(Config::API_IMG_UPLOAD);
            curl_setopt($ch, CURLOPT_HTTP_VERSION,   CURL_HTTP_VERSION_1_0);
            curl_setopt($ch, CURLOPT_TIMEOUT,        60);
            curl_setopt($ch, CURLOPT_HEADER,         false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST,           true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,     http_build_query($params, null, '&'));
            curl_setopt($ch, CURLOPT_PROXY,          Config::PROXY_IP);
            curl_setopt($ch, CURLOPT_PROXYPORT,      Config::PROXY_PORT);

            // respone
            $response = curl_exec($ch);

            if (!empty($response)) {

                $response = json_decode($response, true);

                if (is_array($response) && isset($response[0])
                    && isset($response[0]['url']) && !empty($response[0]['url'])) {

                    return $response[0]['url'];
                }
            }

        } catch (Zend_Exception $e) {
            // TODO: write log
        }

        return false;
    }

    /**
     * @param  string $imageUrl
     * @param  array  $params (url, height, width, crop, quality)
     * @return string $newImageUrl
     */
    public static function requestResize($params) {

        try {

            if (!isset($params['url'])) return false;

            //$config = App::getConfig('proxy');
            $url = 'http://'.$_SERVER['HTTP_HOST'].'/admin/go123/resize/?' . http_build_query($params);
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT,        60);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
           // curl_setopt($ch, CURLOPT_PROXY,          $config['host']);
            //curl_setopt($ch, CURLOPT_PROXYPORT,      $config['port']);

            $data = curl_exec($ch);
            curl_close($ch);

            if (empty($data)) return $params['url'];


            $data = json_decode($data, true);


            if (is_array($data) && isset($data['src']) && !empty($data['src'])
                && isset($data['error']) && $data['error'] == 0) {

                return $data['src'];
            }
        } catch (Zend_Exception $e) {
            // TODO: write log
        }

        return $params['url'];

    }

    public static function resize($imageUrl, $width, $height, $quality = 80, $crop = 1){
        return self::requestResize(array(
            'url'     => $imageUrl,
            'width'   => $width,
            'height'  => $height,
            'quality' => $quality,
            'crop'    => $crop,
        ));
    }

    public static function ranger($url) {
        //$config = App::getConfig('proxy_alt');
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($ch, CURLOPT_PROXY,          $config['host']);
        //curl_setopt($ch, CURLOPT_PROXYPORT,      $config['port']);
        $result = curl_exec($ch);
        $img = imagecreatefromstring($result);
        return $img;
    }

    /**
    * Resize an image and keep the proportions
    * @author Allison Beckwith <allison@planetargon.com>
    * @param string $filename
    * @param integer $max_width
    * @param integer $max_height
    * @return image
    */
    public static function resizeMyImage($file, $w, $h) {
        //Get the original image dimensions + type
        list($source_width, $source_height, $source_type) = getimagesize($file);

        $parts = parse_url($file);
        $path = $parts['path'];
        $path = ltrim($path, '/');
        $uri = explode('/', $path);
        $fileName = explode('.', end($uri))[0];
        //Figure out if we need to create a new JPG, PNG or GIF
        $ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
        if ($ext == "jpg" || $ext == "jpeg") {
            $source_gdim=imagecreatefromjpeg($file);
        } elseif ($ext == "png") {
            $source_gdim=imagecreatefrompng($file);
        } elseif ($ext == "gif") {
            $source_gdim=imagecreatefromgif($file);
        } else {
            //Invalid file type? Return.
            return;
        }
     
        //If a width is supplied, but height is false, then we need to resize by width instead of cropping
        if ($w && !$h) {
            $ratio = $w / $source_width;
            $temp_width = $w;
            $temp_height = $source_height * $ratio;
     
            $desired_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled(
                $desired_gdim,
                $source_gdim,
                0, 0,
                0, 0,
                $temp_width, $temp_height,
                $source_width, $source_height
            );
        } else {
            $source_aspect_ratio = $source_width / $source_height;
            $desired_aspect_ratio = $w / $h;
            if ($source_aspect_ratio > $desired_aspect_ratio) {
                /*
                 * Triggered when source image is wider
                 */
                $temp_height = $h;
                $temp_width = ( int ) ($h * $source_aspect_ratio);
            } else {
                /*
                 * Triggered otherwise (i.e. source image is similar or taller)
                 */
                $temp_width = $w;
                $temp_height = ( int ) ($w / $source_aspect_ratio);
            }
            /*
             * Resize the image into a temporary GD image
             */
            $temp_gdim = imagecreatetruecolor($temp_width, $temp_height);
            imagecopyresampled(
                $temp_gdim,
                $source_gdim,
                0, 0,
                0, 0,
                $temp_width, $temp_height,
                $source_width, $source_height
            );
            /*
             * Copy cropped region from temporary image into the desired GD image
             */
            $x0 = ($temp_width - $w) / 2;
            $y0 = ($temp_height - $h) / 2;
            $desired_gdim = imagecreatetruecolor($w, $h);
            imagecopy(
                $desired_gdim,
                $temp_gdim,
                0, 0,
                $x0, $y0,
                $w, $h
            );
        }

        //Domain

        $upload_path = UPLOAD_PATH;
        $folder = date('Y').'/'.date('m');
        $domain = isset($_SERVER['HTTP_HOST'])?'http://'.$_SERVER['HTTP_HOST']:DOMAIN;
        /*
         * Render the image
         * Alternatively, you can save the image in file-system or database
         */
        $destination = $upload_path.'/upload/' .$folder .'/'. $fileName .'_'. $w .'x'. $h .'.'. $ext;
        $url = '/upload/'.$folder .'/'. $fileName .'_'. $w .'x'. $h .'.'. $ext;
        if ($ext == "jpg" || $ext == "jpeg") {
            ImageJpeg($desired_gdim,$destination,100);
        } elseif ($ext == "png") {
            ImagePng($desired_gdim,$destination);
        } elseif ($ext == "gif") {
            ImageGif($desired_gdim,$destination);
        } else {
            return;
        }
        ImageDestroy ($desired_gdim);
        return $url;
    }
}
