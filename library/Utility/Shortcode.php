 <?php

class Utility_Shortcode {

    protected static $_instance;
    private $tags = array();

    function __construct() {
        $this->tags['muangay'] = array(&$this, 'shortcode_muangay');
        $this->tags['xemtrailer'] = array(&$this, 'shortcode_xemtrailer');
        $this->tags['footerlink'] = array(&$this, 'shortcode_footerlink');
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function convert($content) {
        return self::getInstance()->do_shortcode($content);
    }

    private function do_shortcode($content) {
        $pattern = $this->get_shortcode_regex();
        return preg_replace_callback("/$pattern/s", array(&$this, 'do_shortcode_tag'), $content);
    }

    private function do_shortcode_tag( $m ) {
        // allow [[foo]] syntax for escaping a tag
        if ( $m[1] == '[' && $m[6] == ']' ) {
            return substr($m[0], 1, -1);
        }

        $tag = $m[2];
        $attr = $this->shortcode_parse_atts( $m[3] );

        if ( isset( $m[5] ) ) {
            // enclosing tag - extra parameter
            return $m[1] . call_user_func( $this->tags[$tag], $attr, $m[5], $tag ) . $m[6];
        } else {
            // self-closing tag
            return $m[1] . call_user_func( $this->tags[$tag], $attr, null,  $tag ) . $m[6];
        }
    }

    private function shortcode_parse_atts($text) {
        $atts = array();
        $pattern = '/(\w+)\s*=\s*"([^"]*)"(?:\s|$)|(\w+)\s*=\s*\'([^\']*)\'(?:\s|$)|(\w+)\s*=\s*([^\s\'"]+)(?:\s|$)|"([^"]*)"(?:\s|$)|(\S+)(?:\s|$)/';
        $text = preg_replace("/[\x{00a0}\x{200b}]+/u", " ", $text);
        if ( preg_match_all($pattern, $text, $match, PREG_SET_ORDER) ) {
            foreach ($match as $m) {
                if (!empty($m[1]))
                    $atts[strtolower($m[1])] = stripcslashes($m[2]);
                elseif (!empty($m[3]))
                    $atts[strtolower($m[3])] = stripcslashes($m[4]);
                elseif (!empty($m[5]))
                    $atts[strtolower($m[5])] = stripcslashes($m[6]);
                elseif (isset($m[7]) and strlen($m[7]))
                    $atts[] = stripcslashes($m[7]);
                elseif (isset($m[8]))
                    $atts[] = stripcslashes($m[8]);
            }
        } else {
            $atts = ltrim($text);
        }
        return $atts;
    }

    private function get_shortcode_regex() {
        $tagnames = array_keys($this->tags);
        $tagregexp = join( '|', array_map('preg_quote', $tagnames));

        return
              '\\['                              // Opening bracket
            . '(\\[?)'                           // 1: Optional second opening bracket for escaping shortcodes: [[tag]]
            . "($tagregexp)"                     // 2: Shortcode name
            . '(?![\\w-])'                       // Not followed by word character or hyphen
            . '('                                // 3: Unroll the loop: Inside the opening shortcode tag
            .     '[^\\]\\/]*'                   // Not a closing bracket or forward slash
            .     '(?:'
            .         '\\/(?!\\])'               // A forward slash not followed by a closing bracket
            .         '[^\\]\\/]*'               // Not a closing bracket or forward slash
            .     ')*?'
            . ')'
            . '(?:'
            .     '(\\/)'                        // 4: Self closing tag ...
            .     '\\]'                          // ... and closing bracket
            . '|'
            .     '\\]'                          // Closing bracket
            .     '(?:'
            .         '('                        // 5: Unroll the loop: Optionally, anything between the opening and closing shortcode tags
            .             '[^\\[]*+'             // Not an opening bracket
            .             '(?:'
            .                 '\\[(?!\\/\\2\\])' // An opening bracket not followed by the closing shortcode tag
            .                 '[^\\[]*+'         // Not an opening bracket
            .             ')*+'
            .         ')'
            .         '\\[\\/\\2\\]'             // Closing shortcode tag
            .     ')?'
            . ')'
            . '(\\]?)';                          // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    }

    private function strip_shortcodes( $content ) {
        if (empty($this->tags) || !is_array($this->tags))
            return $content;

        $pattern = $this->get_shortcode_regex();

        return preg_replace_callback( "/$pattern/s", array(&$this, 'strip_shortcode_tag'), $content );
    }

    private function strip_shortcode_tag( $m ) {
        // allow [[foo]] syntax for escaping a tag
        if ( $m[1] == '[' && $m[6] == ']' ) {
            return substr($m[0], 1, -1);
        }

        return $m[1] . $m[6];
    }

    private function shortcode_muangay($atts) {
        if (!isset($atts['href'])) return '';
        $out = '<a';
        if (is_array($atts)) foreach ($atts as $key => $value) {
            $out .= ' ' . $key . '="' . $value . '"';
        }
        $out .= ' class="sc-buynow" id="button-buynow"><img style="display:block;margin-left:auto;margin-right:auto;" src="http://s3.img.edn.vn/123phim/2013/12/400274e4a96bde5f3d96a32d754dd805.png" alt="Mua ngay"></a>';
        return $out;
    }

    private function shortcode_xemtrailer($atts) {
        if (!isset($atts['href'])) return '';
        $out = '<a';
        if (is_array($atts)) foreach ($atts as $key => $value) {
            $out .= ' ' . $key . '="' . $value . '"';
        }
        $out .= ' class="sc-watchtrailer" id="button-watchtrailer"><img style="display:block;margin-left:auto;margin-right:auto;" src="http://s3.img.edn.vn/123phim/2013/12/1f18143937fcd5431c7391b91b7f9801.jpg" alt="Xem trailer"></a>';
        return $out;
    }

    private function shortcode_footerlink($atts) {
        return '<div class="news-detail-footer clearfix"><a href="https://www.facebook.com/123phim" target="_blank"><img class="news-detail-footer-fb" src="http://s2.img.edn.vn/123phim/2013/10/d25f7cff359df115dd09586d24cf3269.png" alt="123Phim trên Facebook" style="float:left;max-width:48%"/></a>'
                . '<a href="http://www.123phim.vn/caidat/" target="_blank"><img class="news-detail-footer-app" src="http://s2.img.edn.vn/123phim/2013/10/783c08a76737c8eb120bbe5e916b752c.png" alt="App 123Phim" style="float:right;max-width:48%"/></a></div>';
    }
}