<?php
class Utility_Date{
    /*
     * FanalyzeDateParam
    *
    * analyze date string from calendar and return server date format
    *
    * @dateParam (string) must in 'd/m/Y - d/m/Y' format
    * @return (array)
    */
    public static function analyzeDateParam($dateParam){
        if( !isset($dateParam) || trim($dateParam)===''){
            $yesterday = date('Y-m-d', mktime(0, 0, 0, date("m"), date("d")-1, date("Y")));
            $firstDayOfMonth = date('Y-m-01', strtotime($yesterday));
            $fromDate = $firstDayOfMonth;
            $toDate = $yesterday;
            $firstDayOfMonth = date('01/m/Y', strtotime($yesterday));
            $yesterday = date('d/m/Y', strtotime($yesterday));
            $dateParam = $firstDayOfMonth.' - '.$yesterday;
        }else{
            $dateStr = explode("-", $dateParam);
            $fromDate = explode("/",$dateStr[0]);
            $fromDate = trim($fromDate[2]).'-'.trim($fromDate[1]).'-'.trim($fromDate[0]);
            $toDate = explode("/",$dateStr[1]);
            $toDate = trim($toDate[2]).'-'.trim($toDate[1]).'-'.trim($toDate[0]);
        }
        $result = array(
            	   'from_date' => $fromDate
                ,'to_date' => $toDate
                ,'date_param'=> $dateParam
        );
        return $result;
    }
}