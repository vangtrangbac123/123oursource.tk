<?php

class Utility_Currency {

	static function toVND($num, $subfix = 'đ') {
        return number_format($num, 0, ',', '.') . $subfix;
	}

    static function toNumber($string) {
        return preg_replace("/[^0-9]/", '', $string);
    }
}