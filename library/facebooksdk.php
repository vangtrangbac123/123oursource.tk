<?php

include_once 'FacebookSDK/facebook.php';

class FacebookSDK extends Facebook {

	protected static $_instance;

	static public function getInstance() {
		if (!self::$_instance) {

			self::$_instance = new self(array(
				'appId'  => FACEBOOK_APP_ID,
				'secret' => FACEBOOK_APP_SECRET,
			));
		}

		return self::$_instance;
	}
}
