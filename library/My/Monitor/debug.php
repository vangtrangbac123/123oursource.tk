<?php
$registry = My_Registry::getInstance();
$memcache = My_Memcache::getInstance();
$requestUrl = (isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : 'http') . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

function debug_merge_query_string($url, $newQuery = array()) {

    $part = parse_url($url);
    $query = array();
    if (isset($part['query'])) {
        parse_str($part['query'], $query);
    }
    $query = array_merge($query, $newQuery);
    $query = count($query) > 0 ? '?' . http_build_query($query) : '';

    $str = '';
    if (isset($part['scheme']) && isset($part['host'])) {
        $str .= $part['scheme'] . '://' . $part['host'];
    }
    if (isset($part['path'])) {
        $str .= $part['path'];
    }
    return $str . $query;
}

$config = $registry->getConfig();
$STATIC_VERSION = isset($config->STATIC_VERSION) ? $config->STATIC_VERSION : 0;
?>
<style>
.debug {
    background-color: #fff;
    padding: 10px;
}
.debug h3 {
    margin: 1em 0 0 0;
    float: left;
}
.debug h3:hover {
    cursor: pointer;
    background-color: rgb(255, 219, 155);
}
.debug .stt {
    width: 50px;
    text-align: center;
}
.debug .info-key {
    width: 150px;
    text-align: right;
    padding-right: 10px;
}
.debug table.border td {
    border: 1px solid #ccc;
    padding: 4px 8px;
}
.debug .db .key {
    width: 100px;
}
.debug .db {
    width: 100%;
}
.debug .db .alert {
    color: red;
}
.debug .action {
    text-align: right;
    width: 200px;
}
.debug a {
    color: inherit;
}
.debug a:hover {
    color: inherit;
    background-color: rgb(255, 219, 155);
}
.debug table {
    width: 100%;
    border-spacing: 0;
    border-collapse: collapse;
}
.debug thead {
    background-color: #ddd;
}
.debug .cache-group {
    width: 100px;
}
.debug .cache-time {
    width: 100px;
}
.debug .cache-memory {
    width: 200px;
}
.debug td.green {
    color: green;
}
</style>
<!-- <script type="text/javascript" src="http://code.jquery.com/jquery-1.10.1.min.js"></script> -->
<script type="text/javascript">
$(function() {
    $('.clicktoshow').click(function() {
        $(this).next().toggle();
    });
});
</script>
<pre class="debug">
<table>
    <tbody>
        <tr>
            <td class="info-key">APPLICATION_ENV</td>
            <td class="value"><?php echo APPLICATION_ENV?></td>
        </tr>
        <tr>
            <td class="info-key">SERVER_IP</td>
            <td class="value"><?php echo $_SERVER['SERVER_ADDR']?></td>
        </tr>
        <tr>
            <td class="info-key">CLIENT_IP</td>
            <td class="value"><?php echo $_SERVER['REMOTE_ADDR']?></td>
        </tr>
        <tr>
            <td class="info-key">PROCESS_TIME</td>
            <td class="value"><?php echo $PROCESS_TIME?> ms</td>
        </tr>
        <tr>
            <td class="info-key">MEMORY_USAGE</td>
            <td class="value"><?php echo $MEMORY_USAGE?></td>
        </tr>
        <tr>
            <td class="info-key">MEMCACHE_ENABLE</td>
            <td class="value"><?php
            if ($memcache->isEnable()) {
                ?>1 <a href="<?php echo debug_merge_query_string($requestUrl, array('cache' => 0))?>">Disable</a><?php
            } else {
                ?>0 <a href="<?php echo debug_merge_query_string($requestUrl, array('cache' => 1))?>">Enable</a><?php
            }
            ?> / <a href="<?php echo debug_merge_query_string($requestUrl, array('cache_refresh' => 1))?>">Refresh</a></td>
        </tr>
        <tr>
            <td class="info-key">MEMCACHE_CONNECTED</td>
            <td class="value"><?php echo $memcache->isConnected()?></td>
        </tr>
        <tr>
            <td class="info-key">STATIC_PATH</td>
            <td class="value"><?php echo Config::STATIC_PATH?></td>
        </tr>
        <tr>
            <td class="info-key">STATIC_VERSION</td>
            <td class="value"><?php echo $STATIC_VERSION?></td>
        </tr>
    </tbody>
</table>

<?php $profile = My_Profiler::getQueryProfiles(); ?>
<h3 class="clicktoshow">DB (<?php echo $profile['total_query']?> hits / <?php echo round($profile['total_sec'], 4)?> sec)</h3>
<table class="border db">
    <thead>
        <tr>
            <th class="stt">No.</th>
            <th>Time</th>
            <th>Query</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($profile['queries'] as $i => $query): ?>
        <tr class="<?php echo ($query['time'] > 0.05) ? 'alert' : ''?>">
            <td class="stt"><?php echo ($i + 1)?></td>
            <td class="key"><?php echo $query['time']?></td>
            <td class="value"><?php echo str_replace("                ", '', $query['sql'])?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<?php $profile = My_Memcache::getProfiles(); ?>
<h3 class="clicktoshow">Caches (<?php echo Utility_Converter::toMemorySize($profile['total_memory'])?> / <?php echo $profile['total_query']?> hits / <?php echo round($profile['total_sec'], 4)?> sec)</h3>
<table class="border db">
    <thead>
        <tr>
            <th class="stt">No.</th>
            <th class="cache-group">Group</th>
            <th class="cache-time">Time</th>
            <th class="cache-memory">Memory</th>
            <th class="cache-key">Key</th>
            <th class="action"></th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; foreach ($profile['queries'] as $type => $rows) foreach ($rows as $query):
            $keyUrl = urlencode(is_array($query['key']) ? http_build_query($query['key']) : $query['key']);
        ?>
        <tr class="<?php echo ($query['time'] > 0.05) ? 'alert' : ''?>">
            <td class="stt"><?php echo ++$i?></td>
            <td class="cache-group"><?php echo $type?></td>
            <td class="cache-time"><?php echo round($query['time'], 4)?></td>
            <td class="cache-memory"><?php echo Utility_Converter::toMemorySize($query['memory'])?></td>
            <td class="cache-key"><a href="/dev/cache/?key=<?php echo $keyUrl?>" target="_blank"><?php echo (is_array($query['key']) ? print_r($query['key'], true) : $query['key'])?></a></td>
            <td class="action"><a href="/dev/cache/?do=delete&amp;key=<?php echo $keyUrl?>" target="_blank">Delete</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>



<?php $profile = My_Apc::getProfiles(); ?>
<h3 class="clicktoshow">APC Caches (<?php echo Utility_Converter::toMemorySize($profile['total_memory'])?> / <?php echo $profile['total_query']?> hits / <?php echo round($profile['total_sec'], 4)?> sec)</h3>
<table class="border db">
    <thead>
        <tr>
            <th class="stt">No.</th>
            <th class="cache-group">Group</th>
            <th class="cache-time">Time</th>
            <th class="cache-memory">Memory</th>
            <th class="cache-key">Key</th>
            <th class="action"></th>
        </tr>
    </thead>
    <tbody>
        <?php $i = 0; foreach ($profile['queries'] as $type => $rows) foreach ($rows as $query):
            $keyUrl = is_array($query['key']) ? http_build_query(array('key' => $query['key'])) : 'key=' . urlencode($query['key']);
        ?>
        <tr class="<?php echo ($query['time'] > 0.05) ? 'alert' : ''?>">
            <td class="stt"><?php echo ++$i?></td>
            <td class="cache-group"><?php echo $type?></td>
            <td class="cache-time"><?php echo round($query['time'], 4)?></td>
            <td class="cache-memory"><?php echo Utility_Converter::toMemorySize($query['memory'])?></td>
            <td class="cache-key"><a href="/dev/apcu/?<?php echo $keyUrl?>" target="_blank"><?php echo (is_array($query['key']) ? print_r($query['key'], true) : $query['key'])?></a></td>
            <td class="action"><a href="/dev/apcu/?<?php echo $keyUrl?>&amp;do=delete" target="_blank">Delete</a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>


<?php
$api = Api_Frontend::getInstance();
$logs = $api->getLogs();
$secs = $api->getTotalTime();
$memo = $api->getTotalMemory();
?>
<h3 class="clicktoshow">API Frontend (<?php echo Utility_Converter::toMemorySize($memo)?> / <?php echo count($logs)?> hits / <?php echo round($secs, 4)?> sec)</h3>
<table class="border">
    <thead>
        <tr>
            <th class="stt">No.</th>
            <th class="cache-group">Group</th>
            <th class="cache-time">Time</th>
            <th class="cache-memory">Memory</th>
            <th>Path</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($logs as $i => $log): ?>
        <tr>
            <td class="stt"><?php echo ($i + 1)?></td>
            <td class="cache-group"><?php echo $log['type']?></td>
            <td class="cache-time"><?php echo round($log['time'], 4)?></td>
            <td class="cache-memory"><?php echo Utility_Converter::toMemorySize($log['memory'])?></td>
            <td><a href="<?php echo $log['path']?>" target="_blank"><?php echo http_build_query($log['params'])?></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>



<?php
$api = Api_Service::getInstance();
$logs = $api->getLogs();
$secs = $api->getTotalTime();
$memo = $api->getTotalMemory();
?>
<h3 class="clicktoshow">API Service (<?php echo Utility_Converter::toMemorySize($memo)?> / <?php echo count($logs)?> hits / <?php echo round($secs, 4)?> sec)</h3>
<table class="border">
    <thead>
        <tr>
            <th class="stt">No.</th>
            <th class="cache-group">Group</th>
            <th class="cache-time">Time</th>
            <th class="cache-memory">Memory</th>
            <th>Path</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($logs as $i => $log): ?>
        <tr>
            <td class="stt"><?php echo ($i + 1)?></td>
            <td class="cache-group"><?php echo $log['type']?></td>
            <td class="cache-time"><?php echo round($log['time'], 4)?></td>
            <td class="cache-memory"><?php echo Utility_Converter::toMemorySize($log['memory'])?></td>
            <td><a href="<?php echo $log['path']?>" target="_blank"><?php echo http_build_query($log['params'])?></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>



<?php
$api = Api_Checkout::getInstance();
$logs = $api->getLogs();
$secs = $api->getTotalTime();
$memo = $api->getTotalMemory();
?>
<h3 class="clicktoshow">API Checkout (<?php echo Utility_Converter::toMemorySize($memo)?> / <?php echo count($logs)?> hits / <?php echo round($secs, 4)?> sec)</h3>
<table class="border">
    <thead>
        <tr>
            <th class="stt">No.</th>
            <th class="cache-group">Group</th>
            <th class="cache-time">Time</th>
            <th class="cache-memory">Memory</th>
            <th>Path</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($logs as $i => $log): ?>
        <tr>
            <td class="stt"><?php echo ($i + 1)?></td>
            <td class="cache-group"><?php echo $log['type']?></td>
            <td class="cache-time"><?php echo round($log['time'], 4)?></td>
            <td class="cache-memory"><?php echo Utility_Converter::toMemorySize($log['memory'])?></td>
            <td><a href="<?php echo $log['path']?>" target="_blank"><?php echo http_build_query($log['params'])?></a></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>





<h3 class="clicktoshow">Cookies (<?php echo Utility_Converter::toMemorySize(strlen(serialize($_COOKIE)))?>)</h3>
<div style="display:none">
<?php print_r($_COOKIE)?>
</div>


<h3 class="clicktoshow">Sessions (<?php echo isset($_SESSION) ? Utility_Converter::toMemorySize(strlen(serialize($_SESSION))) : '0 B'?>)</h3>
<div style="display:none">
<?php isset($_SESSION) ? print_r($_SESSION) : ''?>
</div>


<h3 class="clicktoshow">Included Files (<?php echo count($INCLUDED_FILES)?>)</h3>
<div style="display:none">
<table class="border">
    <tbody>
        <?php foreach ($INCLUDED_FILES as $i => $file):
        $green = !strstr($file, 'Zend') ? 'green' : '';
        ?>
        <tr>
            <td class="stt"><?php echo ($i + 1)?></td>
            <td class="<?php echo $green?>"><?php echo $file?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
</pre>