<?php
header('content-type: text/html; charset=utf-8');
echo '<pre>';
echo "<a href='?delete=all' onclick=\"return confirm('Delete All?')\">Delete All</a>\n\n";

$intsance = My_Memcache::getInstance();
if (!$intsance->isConnected()) {
    $intsance->connect();
}
$i = 0;
$host = isset($_GET['host']) ? trim($_GET['host']) : APPLICATION_HOST;
$pattern = sprintf('/^%s/i', $host);
$isDelete = isset($_GET['delete']);
$memcache = $intsance->getMemcache();
$allSlabs = $memcache->getExtendedStats('slabs');
foreach ($allSlabs as $server => $slabs) {
    foreach ($slabs AS $slabId => $slabMeta) {
        $cdump = @$memcache->getExtendedStats('cachedump', (int)$slabId);
        if (!is_array($cdump)) continue;
        foreach ($cdump AS $keys => $arrVal) {
            if (!is_array($arrVal)) continue;
            foreach ($arrVal AS $k => $v) {
                if (preg_match($pattern, $k)) {
                    $ke = urlencode($k);
                    echo date('Y-m-d H:i:s', $v[1]) . "\t<a href='/dev/cache/?key=$ke&amp;do=delete' target='_blank'>$k</a>" . PHP_EOL;
                    if ($isDelete) {
                        $memcache->delete($k);
                        $i++;
                    }
                }
            }
       }
    }
}

if ($isDelete) {
    header('location: /dev/memcache/');
}
