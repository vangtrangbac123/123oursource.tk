<?php

class My_Memcache {

    protected static $_instance;
    protected $memcache;
    protected $prefix;
    protected $logs;
    protected $isLog       = false;
    protected $isEnable    = true;
    protected $isUpdate    = false;
    protected $isConnected = false;

    public function __construct()
    {
        $config = App::getConfig('memcache');
        $this->prefix   = $config['prefix'];
        $this->logs     = array();
        $this->isLog    = App::isDebug();
        $this->isEnable = $this->checkIsEnable();
        $this->isUpdate = $this->checkIsRefresh();
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public static function getProfiles()
    {
        $time = 0;
        $memory = 0;
        $logs = self::getInstance()->getLogs();
        $totalQuery = count($logs);
        $queries = array();
        foreach ($logs as $log) {
            $time += $log['time'];
            $memory += $log['memory'];
            $type = $log['type'];
            if (!isset($queries[$type])) $queries[$type] = array();
            $queries[$type][] = $log;
        }

        return array(
            'total_query'  => $totalQuery,
            'total_memory' => $memory,
            'total_sec'    => $time,
            'queries'      => $queries,
        );
    }

    public function connect()
    {
        $this->isConnected = false;
        $this->memcache = new Memcache();

        if (!$this->isEnable) {
            return;
        }

        if ($this->isLog) {
            $t1 = microtime(true);
        }

        $config = App::getConfig('memcache');

        try {
            if (@$this->memcache->connect($config['host'], $config['port'], 10)) {
                $this->isConnected = true;
            }
        } catch (Exception $e) {
        }

        if ($this->isLog) {
            $this->logs[] = array(
                'type'   => 'connect',
                'time'   => microtime(true) - $t1,
                'memory' => 0,
                'key'    => $config['host'] . ':' . $config['port'],
                'data'   => $this->isConnected,
            );
        }
    }

    public function getLogs()
    {
        return $this->logs;
    }

    public function getMemcache()
    {
        return $this->memcache;
    }

    public function set($key, $value, $expire = 300)
    {
        if (!$this->memcache) {
            $this->connect();
        }

        if (!$this->isConnected) {
            return false;
        }

        $key = $this->prefix . $key;

        if (!$this->isLog) {
            return $this->memcache->set($key, $value, MEMCACHE_COMPRESSED, $expire);
        }

        $t1 = microtime(true);
        $results = $this->memcache->set($key, $value, MEMCACHE_COMPRESSED, $expire);
        $t2 = microtime(true);

        $this->logs[] = array(
            'type'   => 'set',
            'time'   => $t2 - $t1,
            'memory' => strlen(serialize($value)),
            'expire' => $expire,
            'key'    => $key,
            // 'data'   => $value,
        );

        return $results;
    }

    public function get($key)
    {
        if (!$this->memcache) {
            $this->connect();
        }

        if (!$this->isConnected || $this->isUpdate) {
            return false;
        }

        if (is_array($key)) {
            return $this->getMulti($key);
        }

        $key = $this->prefix . $key;

        if (!$this->isLog) {
            return $this->memcache->get($key);
        }

        $t1 = microtime(true);
        $results = $this->memcache->get($key);
        $t2 = microtime(true);

        $this->logs[] = array(
            'type'   => 'get',
            'time'   => $t2 - $t1,
            'memory' => strlen(serialize($results)),
            'key'    => $key,
            // 'data'   => $results,
        );

        return $results;
    }

    public function getMulti($keys)
    {
        foreach ($keys as $i => $key) {
            $keys[$i] = $this->prefix . $key;
        }

        if (!$this->isLog) {
            return $this->memcache->get($keys);
        }

        $t1 = microtime(true);
        $results = $this->memcache->get($keys);
        $t2 = microtime(true);

        $this->logs[] = array(
            'type'   => 'get-multi',
            'time'   => $t2 - $t1,
            'memory' => strlen(serialize($results)),
            'key'    => $keys,
            'data'   => $results,
        );

        return $results;
    }

    public function delete($key)
    {
        if (!$this->memcache) {
            $this->connect();
        }

        if (!$this->isConnected) {
            return false;
        }

        $key = $this->prefix . $key;

        if (!$this->isLog) {
            return $this->memcache->delete($key);
        }

        $t1 = microtime(true);
        $results = $this->memcache->delete($key);
        $t2 = microtime(true);

        $this->logs[] = array(
            'type'   => 'delete',
            'time'   => $t2 - $t1,
            'memory' => 0,
            'key'    => $key,
            'data'   => $results,
        );

        return $results;
    }

    public function setRefresh($isUpdate) {
        $this->isUpdate = $isUpdate;
    }

    public function setPrefix($prefix) {
        $this->prefix = $prefix;
    }

    public function getPrefix() {
        return $this->prefix;
    }

    public function isConnected()
    {
        return $this->isConnected;
    }

    public function isEnable()
    {
        return $this->isEnable;
    }

    public function isRefresh()
    {
        return $this->isUpdate;
    }

    private function checkIsEnable()
    {
        $flag = 1;

        if (isset($_SERVER['HTTP_X_123F_CACHE'])) {
            $flag = $_SERVER['HTTP_X_123F_CACHE'];
        } elseif (isset($_GET['cache'])) {
            $flag = $_GET['cache'];
        } elseif (isset($_COOKIE['cache'])) {
            $flag = $_COOKIE['cache'];
        }

        return ($flag == 1);
    }

    private function checkIsRefresh()
    {
        $flag = 0;

        if (isset($_SERVER['HTTP_X_123F_CACHE_REFRESH'])) {
            $flag = $_SERVER['HTTP_X_123F_CACHE_REFRESH'];
        } elseif (isset($_GET['cache_refresh'])) {
            $flag = $_GET['cache_refresh'];
        } elseif (isset($_COOKIE['cache_refresh'])) {
            $flag = $_COOKIE['cache_refresh'];
        }

        return ($flag == 1);
    }
}