<?php

class My_View_Report {

    public static function getListCinema($currentId = -1) {
        $list = My_Model_Helperreport::getInstance()->Cinema->getGalaxyCinema();
        ?>
        <select name="cinema_id" class="col-md-2" data-plugin-selectTwo id="cinema_id">
         <option value="-1" <?php if (-1== $currentId) echo 'Selected';?>>All Cinema</option>
        <?php
        foreach ($list as $row) {
            $id = $row->cinema_id;
            $name = $row->cinema_name;
            ?><option value="<?php echo $id?>"<?php echo $currentId == $id ? ' selected="selected"' : ''?>><?php echo $name?></option><?php
        }
        ?>
        </select>
        <?php
    }
    public static function status($currentId = -1) {
        $list = self::statusBooking();
        ?>
        <select name="status_id" class="col-md-2" data-plugin-selectTwo id="status_id">
         <option value="-1" <?php if (-1== $currentId) echo 'Selected';?>>All Status</option>
        <?php
        foreach ($list as $id => $name) {
            ?><option value="<?php echo $id?>"<?php echo $currentId == $id ? ' selected="selected"' : ''?>><?php echo $name?></option><?php
        }
        ?>
        </select>
        <?php
    }

    public static function from_date($date = ''){
        ?>
            <div class="col-md-4"><div style="float:left;margin-right:20px;margin-left:20px">FROM: <input  style="width:100px" class="form-control text-input small-input filter-cs" type="text" id="dashboard-datepicker" value="<?php echo $date;?>"  placeholder="Suất chiếu" name="from" data-plugin-datepicker data-date-format="yyyy-mm-dd"/></div>
       <?php
   }

    public static function to_date($date = ''){
        ?>
           <div style="float:left"> TO: <input class=" form-control text-input small-input filter-cs" style="width: 100px" type="text" id="dashboard-datepicker" value="<?php echo $date;?>"  placeholder="Suất chiếu" name="to" data-plugin-datepicker data-date-format="yyyy-mm-dd"/></div>
        </div>
       <?php
   }

    public static function getListFilmGalaxy($currentId = -1,$date) {

        $list = My_Model_Helperreport::getInstance()->Film->getListFilmGalaxy($date);
        ?>
        <select name="film_id" id="film_id" class="col-md-3" data-plugin-selectTwo>
         <option value="-1" <?php if (-1== $currentId) echo 'Selected';?>>All Film</option>
        <?php
        foreach ($list as $row) {
            $id = $row->film_id;
            $name = $row->film_name;
            ?><option value="<?php echo $id?>"<?php echo $currentId == $id ? ' selected="selected"' : ''?>><?php echo $name?></option><?php
        }
        ?>
        </select>
        <?php
    }
    public static function ticket_code($current = '') {?>
        <div class="col-md-1">
         <input  style="width: 100px;float:left;margin-left:-10px;"  class="form-control" type="text"  value="<?php echo $current;?>"  placeholder="Ticket Code" name="ticket" />
        </div>
    <?php
    }

    private static function statusBooking(){
      return array(
             4 => 'Đặt vé thành công',
             5 => 'Đơn hàng bị hủy',
             6 => 'Đơn hàng Refund');
    }
    public static function getstatusBooking($id){
      $arr = array(
             1 => 'Tạo đơn hàng',
             2 => 'Tạo đơn hàng',
             3 => 'Tạo đơn hàng',
             4 => 'Đặt vé thành công',
             5 => 'Đơn hàng bị hủy',
             6 => 'Đơn hàng Refund');
      return $arr[$id];
    }



}