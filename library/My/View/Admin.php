<?php

class My_View_Admin {

    public static function multicheck($id,$data){
        foreach ($data as $key => $value) {
            if($id == $value->id) return true;
        }
        return false;
    }

    public static function isActive($data, $field = 'is_active') {
        if (!isset($data->{$field})) return '';
        $checked = (int)$data->{$field} == 1 ? 'checked="checked"' : '';
        return '<div class="switch switch-sm switch-primary"><input type="checkbox" data-plugin-ios-switch ' . $checked . ' /></div>';
    }

    public static function isVoice($data, $field = 'is_voice') {
        if (!isset($data->{$field})) return '';
        $checked = (int)$data->{$field} == 1 ? 'checked="checked"' : '';
        return '<div class="switch switch-sm switch-primary"><input type="checkbox" data-plugin-ios-switch ' . $checked . ' /></div>';
    }

    public static function notificationStatus($statusId) {
        switch ($statusId) {
            case 0:
                return '<span class="label label-primary">New</span>';

            case 1:
                return '<span class="label label-success">Success</span>';

            case 2:
                return '<span class="label label-danger">Fail</span>';

            case 10:
                return '<span class="label label-info">In Process</span>';
        }
    }

    public static function agodaCouponStatus($statusId) {
        switch ($statusId) {
            case 1:
                return '<span class="label label-info">New</span>';

            case 2:
                return '<span class="label label-primary">Received</span>';

            case 3:
                return '<span class="label label-success">Used</span>';

            default:
                return '<span class="label label-info">Undefined</span>';
        }
    }
}