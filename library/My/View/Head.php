<?php

class My_View_Head {

    private $view;
    private $version;

    public function __construct($view) {
        $this->view = $view;
        $this->version = 1;
    }

    public function appendJs($path, $prefix = '', $version = false) {
        $this->add($path, $prefix, $version, 'headScript', 'appendFile');
    }

    public function appendCss($path, $prefix = '', $version = false) {
        $this->add($path, $prefix, $version, 'headLink', 'appendStylesheet');
    }

    public function prependJs($path, $prefix = '', $version = false) {
        $this->add($path, $prefix, $version, 'headScript', 'prependFile');
    }

    public function prependCss($path, $prefix = '', $version = false) {
        $this->add($path, $prefix, $version, 'headLink', 'prependStylesheet');
    }

    private function add($path, $prefix, $version, $type = 'headScript', $placement = 'appendFile') {
        $prefix = ($prefix === false) ? '' : $prefix;
        $prefix = ($prefix != '' && App::isHttps()) ? str_replace('http://', 'https://', $prefix) : $prefix;
        $version = ($version === false) ? '' : ('?v=' . (is_null($version) ? $this->version : $version));
        $this->view->$type()->$placement($prefix . $path . $version);
    }
}