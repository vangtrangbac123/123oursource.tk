<?php

class App {

    protected static $instance;
    protected static $isDebug;
    protected static $isSysAdmin;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function isLocal()
    {
        return Config::IS_LOCAL;
    }

    public static function isProduction()
    {
        return Config::IS_PRODUCTION;
    }

    public static function isDevelopment()
    {
        return Config::IS_DEVELOPMENT;
    }

    public static function isDebug()
    {
        if (!self::$isDebug) {
            self::$isDebug = isset($_COOKIE['debug']);
        }
        return self::$isDebug;
    }

    public static function isSysAdmin()
    {
        if (!self::$isSysAdmin) {
            self::$isSysAdmin = isset($_COOKIE['sysadmin']) && $_COOKIE['sysadmin'] == 'phucph';
        }
        return self::$isSysAdmin;
    }

    public static function isAjaxRequest() {
        return (
            (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            ||
            (strstr($_SERVER['REQUEST_URI'], '/ajax'))
        );
    }

    public static function getClientIp() {

        if (isset($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];

        if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];

        if (isset($_SERVER['HTTP_X_FORWARDED']))
            return $_SERVER['HTTP_X_FORWARDED'];

        if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            return $_SERVER['HTTP_FORWARDED_FOR'];

        if (isset($_SERVER['HTTP_FORWARDED']))
            return $_SERVER['HTTP_FORWARDED'];

        if (isset($_SERVER['REMOTE_ADDR']))
            return $_SERVER['REMOTE_ADDR'];

        return 'UNKNOWN';
    }
    public static function facebooksdk()
    {
        require_once 'facebooksdk.php';
        return FacebookSDK::getInstance();
    }
}

if (App::isDebug() && !App::isAjaxRequest()) {
    register_shutdown_function(function() {

        $PROCESS_TIME = round(microtime(true) - REQUEST_TIME_START, 4);
        $MEMORY_USAGE = Utility_Converter::toMemorySize(memory_get_usage() - MEMORY_USAGE);
        $INCLUDED_FILES = get_included_files();

        include 'debug-dump.php';
    });
}
