/*
Navicat MySQL Data Transfer

Source Server         : 128.199.141.76
Source Server Version : 50541
Source Host           : 128.199.141.76:3306
Source Database       : zinkai

Target Server Type    : MYSQL
Target Server Version : 50541
File Encoding         : 65001

Date: 2015-04-30 09:53:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `content` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `youtube_id` int(11) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `is_like` tinyint(4) DEFAULT NULL,
  `is_share` tinyint(4) DEFAULT NULL,
  `is_comment` tinyint(4) DEFAULT NULL,
  `is_start` tinyint(4) NOT NULL,
  `count_view` tinyint(255) DEFAULT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of comment
-- ----------------------------
INSERT INTO `comment` VALUES ('3', null, '2', '1', null, null, null, null, null, '0', null);

-- ----------------------------
-- Table structure for download
-- ----------------------------
DROP TABLE IF EXISTS `download`;
CREATE TABLE `download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of download
-- ----------------------------
INSERT INTO `download` VALUES ('3', 'test', 'testt', 'tesstbac', 'test', '1', '2015-04-28 16:28:20', '2015-04-28 16:28:20', '0');

-- ----------------------------
-- Table structure for report_link
-- ----------------------------
DROP TABLE IF EXISTS `report_link`;
CREATE TABLE `report_link` (
  `youtube_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_report` tinyint(4) NOT NULL DEFAULT '0',
  `date_add` datetime DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`youtube_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of report_link
-- ----------------------------
INSERT INTO `report_link` VALUES ('9', 'https://www.youtube.com/watch?v=nr_YvcW-e4k', 'Jim Sorensen', '1', '0', null, null);
INSERT INTO `report_link` VALUES ('10', 'https://www.youtube.com/watch?v=1e6vGVdc4-I', 'boli bic', '1', '0', null, null);

-- ----------------------------
-- Table structure for report_text
-- ----------------------------
DROP TABLE IF EXISTS `report_text`;
CREATE TABLE `report_text` (
  `report_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_text` text COLLATE utf8_unicode_ci,
  `is_active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  PRIMARY KEY (`report_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of report_text
-- ----------------------------
INSERT INTO `report_text` VALUES ('3', 'This video spam keywords many time in Description. The owner of this video have violated the Guidlines of youtube Community when try to get high position on youtube search result page by spamming', '0', null);
INSERT INTO `report_text` VALUES ('4', 'The owner of this video have spammed keywords \"Dr Phil\" many times in description to get high position on youtube search result page. But the Day was described in Description is not same with the Day of show. I have get wrong result when i search because of this. I\'m very annoyed about this problem.', '0', null);
INSERT INTO `report_text` VALUES ('5', 'This video violates commuity guidelines. It spams keywords \" Dr Phil \" many times in description.  This makes error search result on youtube.', '0', null);
INSERT INTO `report_text` VALUES ('6', 'The owner of this video amused keywords in description and repeated them many times. When i search the Day of any show, I have got wrong result, I have been very annoyed because of this problem', '0', null);
INSERT INTO `report_text` VALUES ('7', 'I hate spam.This video so that spam too many keywords in the description. This causes very unpleasant for me as well as other users when searching video on youtube', '0', null);
INSERT INTO `report_text` VALUES ('8', 'the owner of this video have increased the number of views, likes, comments, or other metric either through the use of automatic systems or by serving up videos to unsuspecting viewers', '0', null);
INSERT INTO `report_text` VALUES ('9', 'The description don\'t refer information provided on this video. The owner of this video spams keywords \" dr Phil \" many times in description to  trick youtube search algorithms. ', '0', null);
INSERT INTO `report_text` VALUES ('10', 'chủ video này đã cố tình đánh lừa hệ thống tìm kiếm của youtube bằng việc dùng hệ thống tự động để tăng views, like, comment. điều này làm sai lệch kết quả tìm kiếm video của tôi trên youtube. Tôi lo ngại điều đó sẽ trở thành tiền lệ xấu, gây ảnh hưởng tới kết quả tìm kiếm của những người dùng khác. Tôi ghét điều đó', '0', null);
INSERT INTO `report_text` VALUES ('11', 'I found that this account violates youtube Turn of Services by running automated system to enhance views count and using misleading text. This causes the wrong result when i search on youtube and make others feel very annoyed', '0', null);
INSERT INTO `report_text` VALUES ('12', 'this video spams keywords \" Dr Phil\" many times in description. this trick youtube search algorithms. ', '0', null);
INSERT INTO `report_text` VALUES ('13', 'this video spams catchphrases \" Dr Phil\" commonly in depiction. this trap youtube seek calculations.', '0', null);
INSERT INTO `report_text` VALUES ('14', 'The proprietor of this video have spammed essential words \"Dr Phil\" ordinarily in portrayal to get high position on youtube query item page. In any case, the Day was depicted in Description is not same with the Day of show. I have get wrong result when i seek due to this. I\'m exceptionally irritated about this issue.', '0', null);
INSERT INTO `report_text` VALUES ('15', 'Who owns this particular online video have spammed keywords \"Dr Phil\" more often than not within information to acquire higher placement in youtube . com research outcome web site. However the Day time had been identified within Information is just not similar using the Day time regarding indicate. I\'ve got receive completely wrong outcome after i research because of this. I am incredibly irritated about this trouble.', '0', null);

-- ----------------------------
-- Table structure for user_admin
-- ----------------------------
DROP TABLE IF EXISTS `user_admin`;
CREATE TABLE `user_admin` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `level` tinyint(4) DEFAULT '0',
  `date_add` timestamp NULL DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT '1',
  `phone` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `is_admin` tinyint(4) NOT NULL,
  `is_content` tinyint(4) NOT NULL,
  `is_cs` tinyint(4) NOT NULL,
  `is_report` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_admin
-- ----------------------------
INSERT INTO `user_admin` VALUES ('1', 'namth', 'namth', 'e10adc3949ba59abbe56e057f20f883e', '0', '2014-12-23 20:58:41', '2014-12-23 20:58:51', '1', '', null, '1', '0', '0', '0');

-- ----------------------------
-- Table structure for user_report
-- ----------------------------
DROP TABLE IF EXISTS `user_report`;
CREATE TABLE `user_report` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pass` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_report
-- ----------------------------
INSERT INTO `user_report` VALUES ('3', null, 'zinkaiuit1@gmail.com', 'youtube2015', '1', null);
INSERT INTO `user_report` VALUES ('4', null, 'zinkaipaul', 'youtube2015', '1', null);

-- ----------------------------
-- Table structure for user_report_link
-- ----------------------------
DROP TABLE IF EXISTS `user_report_link`;
CREATE TABLE `user_report_link` (
  `user_id` int(11) NOT NULL,
  `youtube_id` int(11) NOT NULL,
  `is_report` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `date_add` datetime DEFAULT NULL,
  `date_active` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`youtube_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user_report_link
-- ----------------------------
INSERT INTO `user_report_link` VALUES ('3', '10', '0', null, null);
INSERT INTO `user_report_link` VALUES ('4', '10', '0', null, null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `computer` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('2', 'M01', 'hoainamuit2@gmail.com', '1', null, '2015-04-20 16:50:17');

-- ----------------------------
-- Table structure for youtube
-- ----------------------------
DROP TABLE IF EXISTS `youtube`;
CREATE TABLE `youtube` (
  `youtube_id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_active` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`youtube_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of youtube
-- ----------------------------
INSERT INTO `youtube` VALUES ('1', 'https://www.youtube.com/watch?v=o7Uqe617uuQ', 'Test', '0', null, '2015-04-21 05:45:37');
INSERT INTO `youtube` VALUES ('2', 'https://www.youtube.com/watch?v=Vcpf4gtaZ88', 'akk', '0', null, '2015-04-21 11:08:30');
INSERT INTO `youtube` VALUES ('3', 'https://www.youtube.com/watch?v=7qYiR1HUyqY', 'Dr Phil April 22 2015', '0', null, '2015-04-22 12:35:04');
INSERT INTO `youtube` VALUES ('4', 'https://www.youtube.com/watch?v=RdRSu_f5qS0', 'Dr Phil 22', '0', null, '2015-04-27 12:25:38');
INSERT INTO `youtube` VALUES ('5', 'https://www.youtube.com/watch?v=mvXQtngrzSE', 'Dr Phil April 27', '0', null, '2015-04-27 22:34:15');
INSERT INTO `youtube` VALUES ('6', 'https://www.youtube.com/watch?v=Vkc_ULW-z4s', 'Dr Phil 29 Tu', '0', null, '2015-04-29 12:18:28');
INSERT INTO `youtube` VALUES ('7', 'https://www.youtube.com/watch?v=lG_Meb00f1o', 'Dr Phil 29 Son', '0', null, '2015-04-29 13:26:35');
INSERT INTO `youtube` VALUES ('8', 'https://www.youtube.com/watch?v=vPoFcSariuM', 'dr phil april 29 (s2)', '0', null, '2015-04-29 13:46:36');
