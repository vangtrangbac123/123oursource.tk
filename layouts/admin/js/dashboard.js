function doPlot(selector, data) {
    return $.plot(selector, data, {
        series: {
            lines: {
                show: true,
                fill: false,
                lineWidth: 1,
                fillColor: {
                    colors: [{
                        opacity: 0.6
                    }, {
                        opacity: 0.6
                    }]
                }
            },
            points: {
                show: true
            },
            shadowSize: 0
        },
        grid: {
            hoverable: true,
            clickable: true,
            borderColor: '#EDEDED',
            borderWidth: 1,
            labelMargin: 15,
            backgroundColor: '#FFF'
        },
        yaxis: {
            min: 0
        },
        xaxis: {
            mode: "categories"
        },
        tooltip: true,
        tooltipOpts: {
            content: '%s: %y',
            shifts: {
                x: -60,
                y: 25
            },
            defaultTheme: false
        }
    });
}

$(function() {
    $.get('/ajax/dashboard/?type=device', function(data) {
        doPlot('#report-device', data);
    });

    $.get('/ajax/dashboard/?type=transaction', function(data) {
        doPlot('#report-transaction', data);
    });

    if ($('#report-trans-cineplex').length > 0) {
        $.get('/ajax/dashboard/?type=report_trans_cineplex', function(data) {
            $('#tmpl-report-1').tmpl(data).appendTo('#report-trans-cineplex');
        });

        $.get('/ajax/dashboard/?type=report_trans_payment', function(data) {
            $('#tmpl-report-2').tmpl(data).appendTo('#report-trans-payment');
        });

        $.get('/ajax/dashboard/?type=report_trans_status', function(data) {
            $('#tmpl-report-3').tmpl(data).appendTo('#report-trans-status');
        });
    }
    $('.report_tran_by_payment_type').click(function(){
        $('.report_tran_by_payment_type').removeAttr('style');
        var payment_type = $(this).data('type');
        $(this).css({'color':'red','font-weight':'bold'});
        $('#report-trans-status').find('tr').remove();
         $.get('/ajax/dashboard/?type=report_trans_status_by_payment_type&&payment_type='+payment_type, function(data) {
            $('#tmpl-report-3').tmpl(data).appendTo('#report-trans-status');
        });
    });
});

