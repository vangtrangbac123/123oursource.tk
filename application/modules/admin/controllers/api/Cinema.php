<?php

class Admin_Api_Cinema extends My_Api_Abstract {

    public function getNewVersion($params) {

        if (!isset($params['version_id'])) {
            throw new Exception('Param version_id not found');
        }

        $version = $this->model->AppVersion->getNewVersion($params['version_id']);
        if (!$version) return 0;
        return $version;
    }
     public function getCinema($params) {

        if (!isset($params['id'])) {
            throw new Exception('Param id not found');
        }

        $cinema = $this->model->Cinema->get($params['id']);
        if (!$cinema) return 0;
        return $cinema;
    }
    public function addCinema($params){
       $body   = $this->getPostBody();
       $params = Zend_Json::decode($body);
        $cinema = array(
        'cinema_id'           => (isset($params['cinema_id']))?$params['cinema_id']:'',
        'p_cinema_id'         => intval($params['p_cinema_id']),
        'url_id'              => 1,
        'location_id'         => $params['location_id'],
        'cinema_name'         => trim($params['cinema_name']),
        'cinema_slug'         => $params['cinema_name'],
        'cinema_name_ascii'   => $params['cinema_name'],
        'cinema_name_escape'  => $params['cinema_name'],
        'cinema_address'      => $params['cinema_address'],
        'cinema_description'  => $params['cinema_description'],
        'cinema_phone'        => $params['cinema_phone'],
        'cinema_url'          => $params['cinema_url'],
        'cinema_url_short'    => '',
        'cinema_shortlink'    => $params['cinema_url'],
        'cinema_logo'         => $params['cinema_logo'],
        'number'              => 1,
        'is_active'           => $params['is_active'],
        'avg_price'           => 1,
        'is_booking'          => 1,
        'cinema_latitude'     => $params['cinema_latitude'],
        'cinema_longitude'    => $params['cinema_longitude'],
        'pass_wifi'           => 1,
        'max_seat'            => 1,
        'date_update'         => date('Y-m-d H:i:s'),
        'event_id'            => 1,
        'list_price'          => 1,
        'logo_url'            => 1,
        'meta_title'          => 1,
        'meta_description'    => 1,
        'meta_keyword'        => 1,
        'meta_title_st'       => 1,
        'meta_description_st' => 1,
        'meta_keyword_st'     => 1
        );

        $cinema['date_add'] = date('Y-m-d H:i:s');
        return $cinema;
        $r = $this->model->cinema->save($cinema);
        if($r) return true;
        return false;

    }
    public function editCinema($params){

    }
}
