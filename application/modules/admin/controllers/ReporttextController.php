<?php

class Admin_ReporttextController extends My_Controller_Form {

    public $_form = 'Reporttext';

    public function getListByType($type = 0) {
        $sql = "SELECT * FROM report_text WHERE type_id = $type";
        return $this->model->Reporttext->getRows($sql);
    }

    public function indexAction(){
        $this->view->form = Admin_Model_Form::get($this->_form);
         $this->view->list = $this->getListByType(0);
    }

    public function advertiseAction(){
        $this->_helper->viewRenderer('index');
        $this->view->form = Admin_Model_Form::get($this->_form);
        $this->view->list = $this->getListByType(1);

    }
    public function thumbnailAction(){
        $this->_helper->viewRenderer('index');
        $this->view->form = Admin_Model_Form::get($this->_form);
        $this->view->list = $this->getListByType(2);

    }
    public function scamsAction(){
        $this->_helper->viewRenderer('index');
        $this->view->form = Admin_Model_Form::get($this->_form);
        $this->view->list = $this->getListByType(3);

    }

}