<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['category_name'] = array(
    'label' => 'Product Category Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['number'] = array(
    'label' => 'Number',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['url_short'] = array(
    'label' => 'Url Short',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$listview = array(
    'part' => 'category',
    'colums' => array('#',  'Name',  'Active')
);

$list = array();
$list['model'] = 'Category';
$list['form']  = 'Category';
$list['table'] = 'categories';
$list['primary'] = 'category_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;