<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);


$fields['gallery_name'] = array(
    'label' => 'Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);



$listview = array(
    'part' => 'gallery',
    'colums' => array('#', 'Name', 'Active')
);
$list = array();
$list['model'] = 'Gallery';
$list['form']  = 'Gallery';
$list['table'] = 'gallery';
$list['primary'] = 'gallery_id';
$list['fields'] = $fields;
$list['listview'] = $listview;


return $list;
