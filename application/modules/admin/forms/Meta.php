<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['controller'] = array(
    'label' => 'Controller',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['action'] = array(
    'label' => 'Action',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['meta_title'] = array(
    'label' => 'Meta title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['meta_description'] = array(
    'label' => 'Meta Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['meta_keyword'] = array(
    'label' => 'Meta Keyword',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);


$listview = array(
    'part' => 'meta',
    'colums' => array('#', 'Controller/Action', 'Meta title', 'Active', 'Action')
);

$list = array();
$list['model'] = 'Meta';
$list['form']  = 'Meta';
$list['table'] = 'menu';
$list['primary'] = 'meta_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;