<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['is_camera'] = array(
    'label' => 'Camera',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['is_share'] = array(
    'label' => 'Share',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['is_skip_detail'] = array(
    'label' => 'is_skip_detail',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);


$fields['banner_url'] = array(
    'label' => 'URL',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['banner_link'] = array(
    'label' => 'Link',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['banner_title'] = array(
    'label' => 'Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['button_title'] = array(
    'label' => 'Button Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['button_action'] = array(
    'label' => 'Button Action',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);



$fields['image_vertical'] = array(
    'label' => 'Image Vertical (doc)',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$fields['image_horizontal'] = array(
    'label' => 'Image Horizontal (ngang)',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['image_left_menu'] = array(
    'label' => 'Image Left Menu',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['background_color'] = array(
    'label' => 'Background Color',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);




$fields['number'] = array(
    'label' => 'Number',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['device_id'] = array(
    'label' => 'Device',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListDevice(),
);
$fields['gallery_id'] = array(
    'label' => 'Gallery',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListGallery(),
);
$fields['film_id'] = array(
    'label' => 'Film',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListFilm(),
);

$fields['cinema_id'] = array(
    'label' => 'Cinema',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListCinema(),
);



$listview = array(
    'part' => 'banner_m',
    'colums' => array('#', 'Image', 'Title', 'Position', 'Device', 'Active', 'Action')
);

$list = array();
$list['model'] = 'BannerMobile';
$list['form']  = 'BannerMobile';
$list['table'] = 'banner_mobile';
$list['primary'] = 'banner_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;