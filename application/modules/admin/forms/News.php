<?php

$fields = array();
$sort   = array();

$sort['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => 'sort',
    'list' => Admin_Model_Form::getListActive(),
    'key'  => 'is_active'
);



$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);



$fields['is_hot'] = array(
    'label' => 'Hot',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);



$fields['news_title'] = array(
    'label' => 'Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['type_id'] = array(
    'label' => 'Type',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_SELECT,
    'list' => Admin_Model_Form::getListTypeNews(),
);

$fields['news_description'] = array(
    'label' => 'Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['news_url_short'] = array(
    'label' => 'Short Link',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOXREAD
);



$fields['news_tags'] = array(
    'label' => 'Tags',
    'data'  => Admin_Model_Form::DATA_STRING,
    'type'  => Admin_Model_Form::TYPE_TAGS_INPUT,
);



$fields['number'] = array(
    'label' => 'Number',
    'data'  => Admin_Model_Form::DATA_STRING,
    'type'  => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['@image_1'] = array(
    'label' => 'Image<br/>640x300<br/>300x150<br/>100x100',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_IMAGE,
    'image' => array(
        array(
            'key'    => 'image_1_3',
            'width'  => 640,
            'height' => 300,
            'type'   => 1,
            'size'   => 3,
        ),
        array(
            'key'    => 'image_1_2',
            'width'  => 300,
            'height' => 150,
            'type'   => 1,
            'size'   => 2,
        ),
        array(
            'key'    => 'image_1_1',
            'width'  => 100,
            'height' => 100,
            'type'   => 1,
            'size'   => 1,
        ),
    )
);

$fields['content'] = array(
    'label' => 'Content',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTEDITOR2
);

$fields['meta_title'] = array(
    'label' => 'Meta title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['meta_description'] = array(
    'label' => 'Meta Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);

$fields['meta_keyword'] = array(
    'label' => 'Meta Keyword',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);





$listview = array(
    'part' => 'news',
    'colums' => array('#', 'Image', 'Title',  'Date', 'Active', 'Action')
);

$list = array();
$list['model'] = 'News';
$list['form']  = 'News';
$list['table'] = 'news';
$list['primary'] = 'news_id';
$list['fields'] = $fields;
$list['listview'] = $listview;
$list['sort'] = $sort;

return $list;