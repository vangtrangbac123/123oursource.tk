<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['menu_name'] = array(
    'label' => 'Name',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['number'] = array(
    'label' => 'Number',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['url'] = array(
    'label' => 'Url',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$listview = array(
    'part' => 'menu',
    'colums' => array('#', 'Menu', 'Link', 'Number','Active', 'Action')
);

$list = array();
$list['model'] = 'Menu';
$list['form']  = 'Menu';
$list['table'] = 'menu';
$list['primary'] = 'menu_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;