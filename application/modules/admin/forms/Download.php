<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Download',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['link'] = array(
    'label' => 'Link',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['youtube_id'] = array(
    'label' => 'Youtube ID',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['title'] = array(
    'label' => 'Title',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);

$fields['description'] = array(
    'label' => 'Description',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTAREA
);


$listview = array(
    'part' => 'youtube',
    'colums' => array('#', 'Link', 'Title', 'is_active', 'Action')
);

$list = array();
$list['model'] = 'Download';
$list['form']  = 'download';
$list['table'] = 'download';
$list['primary'] = 'id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;