<?php

$fields = array();

$fields['is_active'] = array(
    'label' => 'Active',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_CHECKBOX,
);

$fields['computer'] = array(
    'label' => 'Computer',
    'data' => Admin_Model_Form::DATA_INT,
    'type' => Admin_Model_Form::TYPE_TEXTBOX,
);

$fields['email'] = array(
    'label' => 'Email',
    'data' => Admin_Model_Form::DATA_STRING,
    'type' => Admin_Model_Form::TYPE_TEXTBOX
);


$listview = array(
    'part' => 'user',
    'colums' => array('#', 'Computer', 'Email', 'Active', 'Action')
);

$list = array();
$list['model'] = 'User';
$list['form']  = 'User';
$list['table'] = 'users';
$list['primary'] = 'user_id';
$list['fields'] = $fields;
$list['listview'] = $listview;

return $list;