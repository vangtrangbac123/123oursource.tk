<?php

class Admin_Model_Room extends My_Model_Abstract {

    protected $_name    = 'rooms';
    protected $_primary = 'room_id';

}