<?php
class Admin_Model_Tag extends My_Model_Abstract {

    protected $_name = 'tags';
    protected $_primary = 'tag_id';

    public function getTagBySlug($slug) {
        $sql = 'SELECT * FROM tags WHERE tag_slug = ?';
        return $this->_db->fetchRow($sql, array($slug));
    }

    public function getTagByName($name) {
        $sql = 'SELECT * FROM tags WHERE tag_name = ?';
        return $this->_db->fetchRow($sql, array($name));
    }

    public function checkTag($name) {
        $row = $this->getTagByName($name);
        if ($row) return $row;

        $slug = Utility_Unicode::get_str_replace($name);

        $row = $this->getTagBySlug($slug);
        if ($row) return $row;

        $tagId = (int)$this->save(array(
            'tag_slug' => $slug,
            'tag_name' => $name,
            'date_add' => date('Y-m-d H:i:s'),
        ));

        return $this->get($tagId);
    }

    public function delProductTag($pId){
        $sql ="DELETE FROM product_tag WHERE product_id = $pId";
        return $this->_excute($sql);
    }
    public function delNewsTag($nId){
        $sql ="DELETE FROM news_tag WHERE news_id = $nId";
        return $this->_excute($sql);
    }
}