<?php

class Admin_Model_User extends My_Model_Abstract {

    protected $_name    = 'users';
    protected $_primary = 'user_id';
    public function getList($yId){
    	$sql = "SELECT
    			u.user_id,
    			u.computer,
    			u.email,
    			IF(c.is_start = 1, 1, 0) is_start
    			FROM users u
    			LEFT JOIN comment c ON c.user_id = u.user_id AND c.youtube_id = :youtube_id
    			WHERE u.is_active = 1 ";
    	return $this->_db->fetchAll($sql, array('youtube_id' =>$yId));
    }

}