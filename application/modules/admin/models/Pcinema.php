<?php

class Admin_Model_Pcinema extends My_Model_Abstract {

    protected $_name    = 'p_cinema';
    protected $_primary = 'p_cinema_id';

    public function getList() {
        $sql = 'SELECT * FROM p_cinema ORDER BY number ASC';
        return $this->_db->fetchAll($sql);
    }
}