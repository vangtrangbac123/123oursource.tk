<?php

class Admin_Model_ImageProduct extends My_Model_Abstract {

    protected $_name    = 'product_image';
    protected $_primary = 'product_id';

    public function check($params){
       $sql = 'SELECT image_id
               FROM   product_image
               WHERE  type_id =:type_id AND product_id =:product_id AND is_active = 1
               ORDER BY is_active DESC,image_id,date_add
               LIMIT 1';
       return $this->_db->fetchOne($sql,array('type_id' => $params['type_id'],'product_id' => $params['product_id']));
    }

    public function delProductImage($data){
      $productId = $data['product_id'];
      $imageId = $data['image_id'];
      $sql = "DELETE FROM product_image WHERE product_id = $productId AND image_id = $imageId";
      $this->_excute($sql);
      $sql = "DELETE FROM images WHERE  image_id = $imageId";
      $this->_excute($sql);
    }
    /*public function updateGallery(){

         $sql ="SELECT DISTINCT f.product_id
                    FROM products f
                    JOIN product_image fi ON fi.product_id = f.product_id
                    JOIN images i ON i.image_id = fi.image_id AND fi.type_id = 7
                ";
        $productId = $this->_db->fetchAll($sql);
        echo json_encode($productId);


        foreach ($productId as $key => $value) {
            $sql ="DELETE
                  FROM
                  product_image
                  WHERE type_id = 2
                        AND product_id = $value->product_id ";
             $this->_db->query($sql);

             $r = $this->update(array('type_id' => 2),array('product_id = ?' => $value->product_id , 'type_id = ?' =>7 ));
        }
        $this->updateImage();

    }

    public function updateImage(){
      $sql ="SELECT image_id FROM  images
                    WHERE type = 7  ORDER BY date_add DESC
                ";

          $image = $this->_db->fetchAll($sql);
          echo json_encode($image);


       foreach ($image as $key => $value) {

          $sql = "SELECT path,size1,size2 FROM images WHERE image_id =:image_id";
          $img = $this->_db->fetchRow($sql,array('image_id' => $value->image_id));

                $data = array();
                $data['size1'] = $img->path;
                $data['size2'] = $img->size1;
                $data['size3'] = $img->size1;
                $data['size4'] = $img->size2;
                $data['type']  = 2;
                $r = $this->_db->update('images',$data,array('image_id = ?' => $value->image_id));
        }
    }*/
}