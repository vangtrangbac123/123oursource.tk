<?php

class Admin_Model_Artist extends My_Model_Abstract {

    protected $_name    = 'artists';
    protected $_primary = 'artist_id';

    public function getList() {
        $sql = 'SELECT artist_id, artist_name FROM artists ORDER BY artist_name ASC';
        return $this->getRows($sql);
    }
}