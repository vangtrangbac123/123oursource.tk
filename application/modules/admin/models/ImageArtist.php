<?php

class Admin_Model_ImageArtist extends My_Model_Abstract {

    protected $_name    = 'artist_image';
    protected $_primary = 'artist_id';

    public function check($params){
       $sql = 'SELECT image_id
               FROM   artist_image
               WHERE  type_id =:type_id AND artist_id =:artist_id AND is_active = 1
               ORDER BY is_active DESC,image_id,date_add
               LIMIT 1';
       return $this->_db->fetchOne($sql,array('type_id' => $params['type_id'],'artist_id' => $params['artist_id']));
    }
}