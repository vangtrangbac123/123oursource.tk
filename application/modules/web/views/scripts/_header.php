<?php
    $this->cats = $this->registry->getListCategory();
?>
<div id="header" class="header-top clearfix" style="top: 0px; display: block;">
        <div class="container">
            <div class="row">
                <!-- #logo -->
                <div id="logo" class="span4 col-md-4">
                        <a href="/" title="Đất xanh group"><img class="logo" src="/layouts/web/images/logo.png" ></a>
                </div>
                <!-- #logo -->
                <!-- navigation-->
                <div class="top-nav-menu col-md-8">
                <div id="skenav" class="ske-menu" style="position: relative;">
                    <ul id="menu-main" class="menu" style="">
                        <li  class="home">
                            <span class="this_child"></span>
                            <a href="/" class="active">Trang chủ</a>
                        </li>
                        <li  id="menu-product" class="product">
                            <span class="this_child"></span>
                            <a href="#">Dự án</a>
                        </li>
                        <li  class="news">
                            <span class="this_child"></span>
                            <a href="/tin-tuc">Tin tức</a>
                        </li>
                        <li  class="intro">
                            <span class="this_child"></span>
                            <a href="/gioi-thieu">Giới thiệu</a>
                        </li>
                        <li  class="contact">
                            <span class="this_child"></span>
                            <a href="/lien-he">Liên hệ</a>
                        </li>
                        <li  class="res">
                            <span class="this_child"></span>
                            <a href="#">Đăng ký</a>
                        </li>
                            </li>
                        </ul>
                    </div> </div>
                <div class="clearfix"></div>
                    <!-- #navigation --> 
            </div>
        </div>
</div>
<div id="sub-menu" class="sub-menu1" style="position:absolute;height:400px;width:100%;z-index:10001;display:none">
        <div class="container">
            <div class="row item" style="background-color:#FFA500;height:25px">
                <div class="col-md-7"></div>
                <div class="col-md-5">
                    <ul>
                    <?php foreach ($this->cats as $id => $cat):?>
                        <li><a href="#" data-panel="panel<?php echo $cat->category_id;?>"><?php echo $cat->category_name;?></a></li>
                    <?php endforeach;?>
                    </ul>
                </div>
            </div>
                <?php echo $this->render('part/header/panel.phtml');?>
        </div>
 </div>



