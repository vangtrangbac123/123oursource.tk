<?php
   $event = $this->registry->getListEvent();
?>
<div class="footer home-footer">
		<div class="news_home" style="padding-bottom:10px;">

         <div class="footer">

            <div class="line_footer"><!-- --></div>
            <div class="coppy">
            	<div class="coppyright" style="margin-top:25px;color:#000">
                	Bản quyền © 2014. <a target="_blank" href="/">Tập đoàn Đất xanh</a>. Ghi rõ nguồn khi khi bạn phát hành từ website này.<br>Thiết kế &amp; phát triển bởi Zinkai.
        </div>
            <div style="position:fixed;bottom:10px;right:10px;z-index:2000"><a href="#">
                <img src="/layouts/web/img/gotop.png" width="50px" height="50px"/>
            </a></div>
             <!--SOCIAL(26/06/2013)-->
            <ul class="social">
            	<li><a target="_blank" href="http://facebook.com"><img src="/layouts/web/img/0.410466001373275141facebook.png"></a></li>

            	<li><a target="_blank" href="http://twitter.com"><img src="/layouts/web/img/0.572257001373275165twitter.png"></a></li>

            	<li><a target="_blank" href="http://mail.yahoo.com"><img src="/layouts/web/img/0.660918001373275179email.png"></a></li>

            	<li><a target="_blank" href="http://linked.com"><img src="/layouts/web/img/0.416950001373275196in-footer.png"></a></li>

            	<li><a target="_blank" href="http://google.com"><img src="/layouts/web/img/0.952131001373275203cong.png"></a></li>
            </ul>
             <!--//SOCIAL(26/06/2013)-->
            <div class="ClearBoth"></div>
        </div>
       <div class="event-calendar block-show" style="right: 0px;">
            <a class="event-toggle"></a>
            <div id="eventCalendarInline" class="eventCalendar-wrap" data-current-month="10" data-current-year="2014"></div>
        </div>

</div>

<script>
        $(function($){

            var strEvent = '<?php echo json_encode($event)?>';
             if (strEvent.length == 0)
            {
                strEvent = "[]";
            }
             //console.log(strEvent);
            var eventsInline = $.parseJSON(strEvent);
           
            if ($("body").attr("data-controller") == "index") {
                $(".event-calendar").animate({ right: "0px" }, 0);
                $(".event-calendar").addClass("block-show");
            }
            else {
                $(".event-calendar").animate({ right: "-189px" }, 0);
            }

            $(".event-toggle").click(function () {
                if ($(".event-calendar").hasClass("block-show")) {
                    $(".event-calendar").animate({ right: "-189px" }, 500);
                    $(".event-calendar").removeClass("block-show");
                }
                else {
                    $(".event-calendar").animate({ right: "0px" }, 500);
                    $(".event-calendar").addClass("block-show");
                }
            });

            $("#eventCalendarInline").eventCalendar({
                jsonData: eventsInline,
                eventsScrollable: true,
                showDescription: true,
                jsonDateFormat: 'human',  // you can use also "human" 'YYYY-MM-DD HH:MM:SS'
                 txt_noEvents: 'Không có sự kiện trong giai đoạn này',
                txt_SpecificEvents_prev: "",
                txt_SpecificEvents_after: "Sự kiện:",
                txt_next: "next",
                txt_prev: "prev",
                txt_NextEvents: "Sự kiện sắp diễn ra:",
                txt_GoToEventUrl: "Xem sự kiện",
                monthNames: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6",
                            "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
                dayNames: ['Chủ nhật', 'Thứ 2', 'Thứ 3', 'Thứ 4',
                    'Thứ 5', 'Thứ 6', 'Thứ 7'],
                dayNamesShort: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6', 'T7'],
            });
        });
    
        $(document).ready(function(){
            $(".banner-adv a").click(function(){
                if($(this).attr('href').indexOf("#")>-1)
                    return false;
            });
            $(".slide-footer").slideUp(0).delay(1500).slideDown(1000);
            $(".menutop ul li").hover(function(){
                if(!$(this).hasClass('active'))
                {
                    var span= $(this).children("span");
                    $(span).stop().animate({
                            height: '100%'
                      }, 150);
                }
            },function(){
                if(!$(this).hasClass('active'))
                {
                    var span= $(this).children("span");
                    $(span).stop().animate({
                        bottom: -30
                  }, 300,function(){$(span).css('bottom','').css('height','0')});
              }
            });
            
            $(".menutop #logo").click(function(){
                window.location.href="/";
            });
            
            //active menu
            var pathName = window.location.pathname.toString();
            if(pathName.indexOf(".html") ==-1 )
            {
                $('.nav ul >  li:first').addClass("active");
            }
            else
            {
                var i = 0;
                var arrPath = pathName.split('/');
                var listMenu = $('.nav ul li > a');
                $.each(listMenu, function () {
                    var href = $(this).attr('href').toString();
                    var arrHref = href.split('/');
                    if (arrPath.length > 2 && arrHref.length > 2) {
                        if (arrPath[2].toString().replace(".html", "") == arrHref[2].toString().replace(".html", "")) {
                            $(this).parent().addClass('active');
                            i++;
                            return false;
                        }
                    }
                });
            }

            //menu
             $('#menu-main li').hover(function(){
                $('#sub-menu').hide();
                $('.sub-menu'+$(this).index()).show();
            });
            $('#sub-menu').hover(function () {
                   $('#menu-product a').addClass('active');
                 },
                 function () {
                   $(this).fadeOut(500);
                   $('#menu-product a').removeClass('active');
                   $('.panel').hide();
            });
            $('#sub-menu .item a').hover(function(){
                $('.panel').hide();
                $('.'+$(this).data('panel')).show();
            });

            $('#menu-main a').removeClass('active');
            var controller = $('body').data('controller');
            var action = $('body').data('action');
            if(controller == 'index'){
                $('.home a').addClass('active');
            }else if(controller == 'news' && (action == 'index' || action == 'detail')){
                $('.news a').addClass('active');
            }else if(controller == 'product'){
                $('.product a').addClass('active');
            }else if(controller == 'news' && action == 'intro' ){
                 $('.intro a').addClass('active');
            }else if(controller == 'news' && action == 'contact' ){
                 $('.contact a').addClass('active');
            }else{

            }

        });
        
    </script>