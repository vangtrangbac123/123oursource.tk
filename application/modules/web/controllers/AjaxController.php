<?php

class Web_AjaxController extends My_Controller_Web {

	public function init() {
		$this->_helper->layout->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		//$this->_request->setActionName('index');
		header('content-type: application/json; charset=utf-8');
		header("Access-Control-Allow-Origin: *");
	}

	public function getlinkAction(){
		header('content-type: application/json; charset=utf-8');
		header("Access-Control-Allow-Origin: *");

		$params = $this->getRequest()->getParams();

		if(!isset($params['user'])){ 
			echo json_encode(false);
			die;
		}

		$link = $this->model->Youtube->getYoutube($params['user']);
        echo json_encode($link);die;
    }

    public function getlinkoneAction(){
		$link = $this->model->Youtube->getYoutubeOne();
        echo json_encode($link);die;
    }

    public function getreportinfoAction(){
    	$data = $this->model->Userreport->getUserReport();
        echo json_encode($data);die;
    }

    public function updatereportinfoAction(){
    	$params = $this->getRequest()->getParams();
    	$this->model->Userreport->updateReport($params);
    	echo json_encode(true);die;
    }

    public function rawAction(){
    	$post = @file_get_contents("php://input");
   		$post = (array)@json_decode($post);
    	$parts = explode("=",$post['link']); 
			//break the string up around the "/" character in $mystring 
		$post['youtube_id'] = $parts[1];
		$post['date_add'] = date('Y-m-d H:i:s');
		$this->model->Rawdata->insertIgnore($post);
    	echo json_encode($post);die;

    }

	public function indexAction() {

		$params = $this->getRequest()->getParams();

		if (!isset($params['method'])) die('Request Invalid');

		list($object, $function) = explode('.', $params['method'], 2);

		$path = dirname(__FILE__);

		$file    = "$path/ajax/$object.php";

		if (!file_exists($file)) die('Object not found');

		require_once $file;

		$controller = "Android_Ajax_$object";

		if (!class_exists($controller)) die('Class not found');

		$service = new $controller($object, $this);

		if (!method_exists($service, $function)) die(0); // Function not found

		unset($params['method']);
		unset($params['module']);
		unset($params['action']);
		unset($params['controller']);

		$results = call_user_func(array($service, $function), $params);

		header('content-type: application/json; charset=utf-8');
		echo Zend_Json::encode($results);
		die;
	}

}
