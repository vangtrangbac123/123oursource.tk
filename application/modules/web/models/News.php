<?php

class Web_Model_News extends My_Model_Abstract {

    protected $_name    = 'news';
    protected $_primary = 'news_id';

    public function getList($offset = 0,$count = 5) {
        $sql = "SELECT SQl_CALC_FOUND_ROWS n.*, i.size2
                FROM news n
                LEFT JOIN news_image ni ON ni.news_id = n.news_id AND ni.is_active = 1
                LEFT JOIN images i ON i.image_id = ni.image_id
                WHERE  n.is_active = 1 AND n.type_id = 1
                ORDER BY n.date_add DESC
                LIMIT $offset, $count
                ";

        $sql2 = "SELECT FOUND_ROWS()";

        $rows =  $this->_db->fetchAll($sql);

        $total = (int)$this->_db->fetchOne($sql2);

        return array('rows' => $rows, 'total' => $total);
    }

    public function getDetail($newsId){
        $sql = "SELECT  *
               FROM news
               WHERE  news_id = :news_id";
        return $this->_db->fetchRow($sql, array('news_id' => $newsId));
    }

    public function getNewsIntro(){
        $sql = "SELECT  *
               FROM news
               WHERE  type_id = 2
               ORDER BY date_add DESC
               LIMIT 1 ";
        return $this->_db->fetchRow($sql);
    }

    public function getNewsContact(){
        $sql = "SELECT  *
               FROM news
               WHERE  type_id = 4
               ORDER BY date_add DESC
               LIMIT 1 ";
        return $this->_db->fetchRow($sql);
    }

    public function getListRelate($data){
        $sql = "SELECT  *
               FROM news
               WHERE  news_id <> :news_id 
                     AND type_id = :type_id
                     AND is_active = 1
                ORDER BY is_hot DESC,date_add DESC
                LIMIT 10";
        return $this->_db->fetchAll($sql, array('news_id' => $data['news_id'],'type_id' => $data['type_id']));
    }


}